import React from 'react'
import { useFormik } from 'formik'

const FormAddAnnonce = () => {


    const formik = useFormik({
        initialValues: {
            titre : '',
            contenu : '',
            dateParution : '',
            localisation : '',
            tarif : 0,
        }
    })

    return (
        <div>
            <form>
                <p>titre : <input type='text' id='titre' name='titre' onChange={formik.handleChange} value={formik.values.name} /></p>
                <p>message de votre annonce : <textarea id='contenu' name='contenu' onChange={formik.handleChange} value={formik.values.name} /></p>
                <p>date parution : <input type="date" id='dateParution' name='dateParution' /></p>
            </form>
        </div>
    )
}

export default FormAddAnnonce
