--------------------------------------------------------------------------------------
				Modules à Installer
--------------------------------------------------------------------------------------

--------- modules de base ---------
--> installe node_modules , contient les packages nécessaire au démarrage de React
commande : npm install

--------- routage avec react ------
--> installe le routage , permet de naviguer d'une page à une autre via un lien
commande : npm install react-router-dom

--------- tailwind ------
--> permet d'utiliser les fonctionnalité de tailwind
comment l'installer : https://tailwindcss.com/docs/guides/create-react-app
solution au probleme du autoprefixer : 
commandes:    npm uninstall tailwindcss postcss autoprefixer
              npm install tailwindcss@latest postcss@latest autoprefixer@latest

              npx tailwindcss init -p

              npm uninstall tailwindcss postcss autoprefixer
              npm install tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9
les modules nécessaire sur un projet déjà créé :
commandes :   npm install -D tailwindcss@npm:@tailwindcss/postcss7-compat postcss@^7 autoprefixer@^9
	      npm install @craco/craco

---------- axios --------
--> Permet de communiquer avec l'api REST
commande : npm install axios

------formik et yup--------
--> Outils pour les formulaires
commandes : 	npm i formik
		npm i yup

---collapse-----
--> permet d'adoucir l'affichage du menu déroulant
npm install @kunukn/react-collapse

----@headlessui--------
--> composants avec tailwind
npm install @headlessui/react

----@heroicons--------
--> composants avec tailwind
npm i heroicons






	      	