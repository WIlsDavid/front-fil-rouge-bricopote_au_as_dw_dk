import './App.css';
import CatGrid from './components/composantsGrilleCategory/CatGrid'
import { Link } from "react-router-dom"; //npm install react-router-dom
import { useState, useEffect } from 'react';
import Axios from 'axios';
import { CategorieContext } from './components/CategorieContext'
import InfoCard from './components/composantsHomePage/InfoCard';
import InfoBricopoteCard from './components/composantsHomePage/InfoBricopoteCard';
import LogoBricoPoteBanner from './Logo/LogoBricoPoteBanner.svg'
import Footer from './components/composantsFooter/Footer';


const App = props => {

  const [categories, setCategories] = useState([]);
  const [reload, setReload] = useState(true);

  useEffect(() => {
    Axios.get("http://localhost:8080/api/categories").then(response => {
      setCategories(response.data);
    })
  }, [reload])

  return (
    <div className="App">
      <div className="bg-image flex-col justify-center py-8">
        <div className="flex justify-center m-8" >
          <img src={LogoBricoPoteBanner}/>
        </div>
        <div>
          <Link to="/search" className="m-10 w-5/12 rounded-full bg-yellowBricopote text-white text-xl px-6">Recherchez votre BricoPote!</Link>
        </div>
        
        
        
      </div>
        

      <div className="flex justify-center my-12">
        <CategorieContext.Provider value={categories}>
          <CatGrid />
        </CategorieContext.Provider>
      </div>

      <div className="flex justify-center">
        <div id="comment ça marche ?" className="lg:w-8/12 bg-font my-8">
          <h1 className="text-3xl font-bold">Comment ça marche ?</h1>
          <div className='flex flex-col lg:flex-row lg:justify-around'>
            <InfoCard image="selectCat" texte="Selectionner Votre Catégorie" />
            <InfoCard image="posterAnnonce" texte="Poster Votre Annonce !" />
            <InfoCard image="recevezReponse" texte="Recevez la réponse des Bricopotes les plus proche !" />
          </div>
        </div>
      </div>

      <div className="flex justify-center">
        <div id="card-devenir-bricopote" className="w-full lg:w-8/12 bg-font grid grid-cols-1 sm:grid-cols-2">
          <InfoBricopoteCard titre="Comment devenir bricopote ?" image="devBricopote" texte="Completez vos compétences ici !" />
          <InfoBricopoteCard titre="Pourquoi devenir bricopote ?" image="pqBricopote" texte="Rendez votre passion du bricolage rentable ! Fixez vos propres prix!" />
        </div>
      </div>

      


    </div>
  );
}

export default App;
