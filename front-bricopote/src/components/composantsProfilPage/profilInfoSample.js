const profilInfoSample =
{
    "idUser": 98,
    "nom": "Tofanelli",
    "prenom": "Wallache",
    "tel": "5317435719",
    "email": "wtofanelli2p@msn.com",
    "imageProfil": null,
    "adresse": {
        "cp": "81423",
        "ville": "Marseille"
    }
}

export default profilInfoSample;