import React, { useState, Fragment } from 'react'
import Collapse from "@kunukn/react-collapse";

const CategorieListItem = ({ cat, categorieActive , setCategorieActive , setlistCompetence , listCompetence }) => {

    const [subCategoriesVisibility, setSubCategoriesVisibility] = useState(false);



    const catfieldClick = (catCode) => {
        (catCode == categorieActive)? setCategorieActive("") : setCategorieActive(catCode)
    }


    const addSousCatToList = (scat,e) => {
        const index = listCompetence.indexOf(scat)
        if (index > -1) {
            setlistCompetence(listCompetence.filter(scat2 => scat2.intitule !== scat.intitule))
        } else {
            setlistCompetence(prevSousCatList => [...prevSousCatList, scat])
        }
    }

    return (
        <Fragment >
            <div>
                
                <div onClick={() => catfieldClick(cat.codeCategorie)} className={`w-96 h-14 bg-${cat.codeCategorie.toLowerCase()} text-xl font-bold text-gray-100 hover:bg-gray-100 hover:text-${cat.codeCategorie.toLowerCase()}`}>
                    {cat.intitule} <img className="w-8 relative left-80 bottom-4" src={`${process.env.PUBLIC_URL}/img/img-${cat.codeCategorie.toLowerCase()}.png`} />
                </div>
                <Collapse 
                    isOpen={cat.codeCategorie == categorieActive}
                    transition="height 1000ms cubic-bezier(.4, 0, .2, 1)"
                >
                    <ul  >
                        {cat.listSousCategories.map((scat) => <li className={`w-96 bg-${scat.categorie.codeCategorie.toLowerCase()} text-gray-100 border-2 w-80 h-16 justify-between`}><input type="checkbox" name="checkboxSC" value={scat.codeSousCategorie} className="" onClick={(e) => addSousCatToList(scat,e)} />   {scat.intitule}
                        </li>)}
                    </ul>
                </Collapse>
            </div>
        </Fragment>
    )
}

export default CategorieListItem
