import React , {useState, useEffect} from 'react'
import Axios from 'axios'
import GroupeCompetence from './GroupeCompetence';

const InventaireCompetence = props => {

    const [categories, setCategories] = useState([]);
    const [reload, setReload] = useState(true);

    useEffect(() => {
        Axios.get("http://localhost:8080/api/categories").then(response => {
            setCategories(response.data);
        })
    }, [reload])

    // useEffect(() => {
    //     {props.sousCatList.sort((a,b)=>{
    //         if(a.categorie.codeCategorie > b.categorie.codeCategorie ){
    //             return 1;
    //         }
    //         if(a.categorie.codeCategorie < b.categorie.codeCategorie ){
    //             return -1;
    //         }
    //         return 0;
    //     })}
    // }, [reload])



    return (
        <div className="grid grid-cols-1 ">

            {categories.map((cat,index) => <GroupeCompetence cat={cat} scats={props.sousCatList} />)}
            
        </div>
    )
}

export default InventaireCompetence
