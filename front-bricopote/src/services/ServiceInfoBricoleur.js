import  axios from "axios";

const API_URL = "http://localhost:8080/api/"

class ServiceSearchInfoBricoleur {

    findUserInfo(id){
        return axios.get(API_URL +"user/search-id/"+ id);
    }

    findSkills(id){
        return axios.get(API_URL +"souscategories/bricoleur/"+ id);
    }

}
export default new ServiceSearchInfoBricoleur();