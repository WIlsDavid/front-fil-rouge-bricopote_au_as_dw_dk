import  axios from "axios";

const API_URL = "http://localhost:8080/api/"

class ServiceAuth {

    createBricopote(user){
    
        return axios.post(API_URL +"bricopote/inscription" , user);
    }

    createUser(user){
        return axios.post(API_URL + "user/inscription" , user);

    }

}

export default new ServiceAuth();