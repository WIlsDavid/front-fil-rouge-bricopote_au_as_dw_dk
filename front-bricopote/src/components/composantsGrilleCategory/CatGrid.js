import React, {Fragment, useEffect, useState , useContext} from 'react'
import CatCards from './CatCards'
import { CategorieContext } from '../CategorieContext'


const CatGrid = props => {

    const categories = useContext(CategorieContext)

    // const [categories, setCategories] = useState([]);
    // const [reload, setReload] = useState(true);

    // useEffect(() => {
    //     Axios.get("http://localhost:8080/api/categories").then(response => {
    //         setCategories(response.data);
    //     })
    // }, [reload])

    // console.log(props);


    return (
        <div className="w-full flex justify-center">
            <div className=" w-full px-auto xl:w-9/12  xl:px-40 py-8 bg-font grid grid-cols-1 md:grid-cols-2 md:px-36 lg:grid-cols-3 gap-y-3" >
            {categories.map(cat => <CatCards cat={cat}/>)}
        </div>
        </div>
        

    )
}

export default CatGrid
