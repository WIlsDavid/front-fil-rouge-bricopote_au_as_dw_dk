import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'; // npm install react-router-dom
import DashBoardCompetence from './components/composantsChoisirCompetences/DashBoardCompetence';
import NotFound from './components/NotFound';
import VueBricoleurs from './components/VueBricoleurs';
import SearchUser from './components/BricoleurSearch/SearchUser';
import RegisterForm from './components/registerForm/registerForm';
import ProfilPage from './components/composantsProfilPage/ProfilPage'
import NavBar from './components/composantsNavbar/Navbar';
import StarRatingDemo from './components/composantsProfilPage/StarRating';
import Footer from './components/composantsFooter/Footer';
import ScrollToTop from './ScrollToTop';

const Root = () => (

  <Router>
    <ScrollToTop/>
    <NavBar />
    <Switch>
      <Route exact path='/' component={App} />
      <Route path='/profil/:id' component={ProfilPage} />
      <Route exact path='/demo' component={StarRatingDemo} />
      <Route exact path='/chooseCat' component={DashBoardCompetence} />
      <Route exact path='/vue-bricoleurs' component={VueBricoleurs} />
      <Route exact path='/search' component={SearchUser} />
      <Route exact path='/inscription/:id' component={RegisterForm} />
      <Route component={NotFound} />
    </Switch>
    <Footer />
  </Router>
);

ReactDOM.render(<Root />, document.getElementById('root'));


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
