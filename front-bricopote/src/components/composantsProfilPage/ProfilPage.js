import React, {useState,useEffect} from 'react'
import InventaireCompetence from '../composantsChoisirCompetences/InventaireCompetence'
import samplePhoto from "./assets/img/sample-photo.jpg"
import StarRating from "./StarRating"
import outils from "./assets/img/tools.svg"
import BtnModal  from "./BtnModal"
import Modal3  from "./Modal3"
import BtnModalFormulaire  from "./BtnModalFormulaire"
import ServiceInfoBricoleur from "../../services/ServiceInfoBricoleur"



const ProfilPage = (props) => {

    const userId = props.match.params.id
    const [profil, setProfil] = useState([])
    const [listCompetence, setListCompetence] = useState([])

    useEffect(() => {
        ServiceInfoBricoleur.findUserInfo(userId)
        .then(response => {
            setProfil(response.data);
        })
        .catch(function (erreur) {
            console.log(erreur);
        });

        ServiceInfoBricoleur.findSkills(userId)
        .then(response => {
            setListCompetence(response.data);
        })
        .catch(function (erreur) {
            console.log(erreur);
        });
    },[])


    return (
        <div>
            <div class="bg-gray-100">
                <div class="container mx-auto my-2 p-5">
                    <div class="md:flex no-wrap md:-mx-2 ">
                        {/* <!-- Left Side --> */}
            <div class="w-full md:w-3/12 md:mx-2">
                            {/* <!-- Profile Card --> */}
                <div class="bg-white p-3 border-t-4 border-yellowBricopote">
                                <div class="image">
                                    <img class="w-72 h-96 rounded-md object-cover mx-auto"
                                        src={samplePhoto}
                                        alt=""/>
                    </div>
                                    <h1 class="text-gray-900 font-bold text-xl leading-8 my-1">{profil.prenom + " " + profil.nom}</h1>

                                    <div className="grid grid-col-1 mb-2">
                                        <StarRating value={3} size={40}/>
                                        <Modal3 btnText="contacter par email" info={profil.email}/>
                                        <BtnModal btnText="contacter par téléphone" info={profil.tel}/>
                                        <BtnModalFormulaire btnText="envoyer un message" info="info@bricopote.com"/>
                                    </div>
                                    
                                    
                            
                                    <p class="text-sm text-gray-500 hover:text-gray-600 leading-6">Lorem ipsum dolor sit amet
                                    consectetur adipisicing elit.
                        Reprehenderit</p>
                                    <ul
                                        class="bg-gray-100 text-gray-600 hover:text-gray-700 hover:shadow py-2 px-3 mt-3 divide-y rounded shadow-sm">
                                        <li class="flex items-center py-3">
                                            <span>Ville </span>
                                            <span class="ml-auto">{(profil.adresse) && (profil.adresse.ville)}</span>
                                        </li>

                                        <li class="flex items-center py-3">
                                            <span>Code Postal </span>
                                            <span class="ml-auto">{(profil.adresse) && (profil.adresse.cp)}</span>
                                        </li>
                                        
                                        <li class="flex items-center py-3">
                                            <span>Membre depuis</span>
                                            <span class="ml-auto">07 Nov 2016</span>
                                        </li>
                                    </ul>
                                </div>
                                {/* <!-- End of profile card --> */}
                <div class="my-4"></div>
            </div>
                                            {/* <!-- Right Side --> */}
            <div class="w-full md:w-8/12 mx-2 h-64">
                                                {/* <!-- Profile tab --> */}
                {/* <!-- Competences Section --> */}
                <div class="bg-white p-3 shadow-sm rounded-sm">
                                                    <div class="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
                                                        <img className="w-6" src={outils}/>
                                                        <h1 className="tracking-wide">Compétences</h1>
                                                    </div>
                                                    <InventaireCompetence sousCatList={listCompetence} />
                                                </div>
                                                {/* <!-- End of Competences section --> */}

                <div class="my-4"></div>

                                                {/* <!-- Experience and education --> */}
                <div class="bg-white p-3 shadow-sm rounded-sm">
                                                    
                                                    {/* <!-- End of Experience and education grid --> */}
                </div>
                                                {/* <!-- End of profile tab --> */}
            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    )
}

export default ProfilPage
