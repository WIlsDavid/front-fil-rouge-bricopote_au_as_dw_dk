import React, { useState, useEffect, Fragment } from 'react'
import Axios from 'axios'
import { Link } from "react-router-dom";
import CategorieListItem from './CategorieListItem';
import InventaireCompetence from './InventaireCompetence';
import { CategorieContext } from '../CategorieContext'


const SelectionCategorie = ({listCompetence, setlistCompetence}) => {

    const [categories, setCategories] = useState([]);
    const [sousCategories, setSousCategories] = useState([]);
    const [reload, setReload] = useState(true);
    const [categorieActive , setCategorieActive] = useState(true);


    useEffect(() => {
        Axios.get("http://localhost:8080/api/categories").then(response => {
            setCategories(response.data);
        })
    }, [reload])

    useEffect(() => {
        Axios.get("http://localhost:8080/api/souscategories").then(response => {
            setSousCategories(response.data);
        })
    }, [reload])

    return (
        <div className="flex-row">
            <div className="flex justify-around">
                <div>
                    <ul>
                        {categories.map((cat, index) => <li key={index} ><CategorieListItem categorieActive={categorieActive} setCategorieActive={setCategorieActive} cat={cat} setlistCompetence={setlistCompetence} listCompetence={listCompetence} /></li>)}
                    </ul>
                </div>
                
            </div>

        </div>

    )
}

export default SelectionCategorie
