import React , {useState, useEffect} from 'react'
import SelectionCategorie from './SelectionCategorie'
import Axios from 'axios'
import { Link } from "react-router-dom";
import InventaireCompetence from './InventaireCompetence';

const DashBoardCompetence = props => {

    const [categories, setCategories] = useState([]);
    const [listCompetence, setlistCompetence] = useState([])
    const [reload, setReload] = useState(true);

    useEffect(() => {
        Axios.get("http://localhost:8080/api/categories").then(response => {
            setCategories(response.data);
        })
    }, [reload])

    return (
        <div>
            <div className="flex justify-around">
                <SelectionCategorie listCompetence={listCompetence} setlistCompetence={setlistCompetence}/>

                <InventaireCompetence sousCatList={listCompetence} categories={categories} />
            </div>
        </div>
    )
}

export default DashBoardCompetence
