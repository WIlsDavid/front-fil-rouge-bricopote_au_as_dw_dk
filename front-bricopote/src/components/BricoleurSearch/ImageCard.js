import React from 'react'

function ImageCard() {
    return <img alt="avatar" className="w-full h-56 object-cover object-center" src="https://avatars.githubusercontent.com/u/499550?v=4" />;
}

export default ImageCard
