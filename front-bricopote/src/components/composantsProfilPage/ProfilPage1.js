import React, { useState } from "react";
import InventaireCompetence from "../composantsChoisirCompetences/InventaireCompetence";
import samplePhoto from "./assets/img/sample-photo.jpg"
import listCompetence from "./listeCompetencesSample"
import StarRating from './StarRating'
import Modal from './BtnModal'

const ProfilPage = () => {

    return (
        <>
            <div className="bg-gray-600">
                <div className="container m-auto flex">
                    <div className="bg-white w-full mb-6 shadow-xl rounded-lg mt-12  p-12 grid grid-cols-4">
                        <div className="grid grid-cols-1">
                            <img
                                alt="..."
                                src={samplePhoto}
                                className="shadow-2xl rounded-xl max-w-xs h-auto"

                            />
                            <h1 className="text-4xl font-semibold leading-normal text-gray-800 mb-2">John Stones</h1>
                            
                            <StarRating value={3} size={40}/>
                            <div className="flex justify-center py-4 lg:pt-4 pt-8">
                                <div className="mr-4 p-3 text-center">
                                    <span className="text-2xl font-bold block uppercase tracking-wide text-gray-700">
                                        22
                                    </span>
                                    <span className="text-mg text-gray-500">Réalisations</span>
                                </div>
                                <div className="lg:mr-4 p-3 text-center">
                                    <span className="text-2xl font-bold block uppercase tracking-wide text-gray-700">
                                        12
                        </span>
                                    <span className="text-md text-gray-500">Commentaires</span>
                                </div>
                            </div>
                            <Modal color=""/>
                            <button
                                className="bg-yellowBricopote uppercase text-white font-bold hover:shadow-md shadow text-xs px-2 py-2 rounded lg:h-12"
                                
                            >
                                contacter par email
                                            </button>
                            <button
                                className="bg-yellowBricopote uppercase text-white font-bold hover:shadow-md shadow text-xs px-2 py-2 rounded lg:h-12"
                            >
                                contacter par telephone
                            </button>
                            <button
                                className="bg-yellowBricopote uppercase text-white font-bold hover:shadow-md shadow text-xs px-2 py-2 rounded lg:h-12"
                            >
                                envoyer un message
                            </button>
                        </div>

                    <div className="text-center mt-12 col-span-3">
                        
                        
                        <div className="mb-2 text-gray-700 mt-10">
                    Compétences :
                </div>
                        <div className="mb-2 text-gray-700 flex justify-end">
                            <InventaireCompetence sousCatList={listCompetence} />
                        </div>
                    </div>
                    <div className="mt-10 py-10 border-t border-gray-300 text-center col-span-4">
                        <div className="flex flex-wrap justify-center">
                            <div className="w-full lg:w-9/12 px-4">
                                <p className="mb-4 text-lg leading-relaxed text-gray-800">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla delectus cumque facilis maiores eveniet! Aspernatur numquam praesentium, voluptatum, assumenda corporis asperiores voluptas optio, maiores temporibus repellat enim illo vitae quas.
                                            </p>
                                <p
                                    className="font-normal text-yellow-500"
                                    onClick={e => e.preventDefault()}
                                >
                                    Show more
                                            </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> </div>
        </>
    );
}
export default ProfilPage
