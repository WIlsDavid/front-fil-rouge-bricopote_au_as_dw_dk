const listeCompetencesSample = [
    {
        "idSousCategorie": 41,
        "intitule": "Transport",
        "codeSousCategorie": "TRA",
        "categorie": {
            "idCategorie": 7,
            "intitule": "Déménagement",
            "codeCategorie": "DEM"
        }
    },
    {
        "idSousCategorie": 46,
        "intitule": "Maintenance et réparation",
        "codeSousCategorie": "MAR",
        "categorie": {
            "idCategorie": 9,
            "intitule": "Informatique",
            "codeCategorie": "INF"
        }
    },
    {
        "idSousCategorie": 12,
        "intitule": "Rénovation mur",
        "codeSousCategorie": "RMU",
        "categorie": {
            "idCategorie": 2,
            "intitule": "Peinture",
            "codeCategorie": "PEI"
        }
    },
    {
        "idSousCategorie": 26,
        "intitule": "Installation électrique",
        "codeSousCategorie": "INE",
        "categorie": {
            "idCategorie": 4,
            "intitule": "Electricité",
            "codeCategorie": "ELE"
        }
    },
    {
        "idSousCategorie": 25,
        "intitule": "Installation luminaire",
        "codeSousCategorie": "INL",
        "categorie": {
            "idCategorie": 4,
            "intitule": "Electricité",
            "codeCategorie": "ELE"
        }
    },
    {
        "idSousCategorie": 43,
        "intitule": "Installation objet de déco",
        "codeSousCategorie": "IOD",
        "categorie": {
            "idCategorie": 8,
            "intitule": "Montage de meubles",
            "codeCategorie": "MON"
        }
    }
]

export default listeCompetencesSample;