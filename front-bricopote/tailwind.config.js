const colors = require('tailwindcss/colors');


module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        jar : '#bfb5f3',
        elm : '#FFC500',
        mon : '#4B22BF',
        plo : '#E5A872',
        inf : '#DB3126',
        ele : '#17659D',
        pei : '#FFB8B8',
        dem : '#008080',
        brm : '#909090',
        font : '#FFF3F3',
        yellowBricopote : '#F2BC37'
      },
      fontFamily: {
        'sans' : ['Anton', 'Helvetica','Arial','sans-serif']
      }
    },
    
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
