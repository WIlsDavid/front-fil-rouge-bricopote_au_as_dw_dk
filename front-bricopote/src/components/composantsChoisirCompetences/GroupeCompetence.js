import React, {useState,useEffect} from 'react'

const GroupeCompetence = ({cat, scats}) => {

    // filtre les sous-categories correspondantes à la Categorie
    const filteredList = scats.filter(scat => scat.categorie.intitule == cat.intitule)
    const [showCategorie, setShowCategorie] = useState(true)

    const catColor = cat.codeCategorie.toLowerCase()

    useEffect(() => {
        (filteredList.length > 0) ? setShowCategorie(true) : setShowCategorie(false)
    })
    

    return (
        <div>
            {console.log(scats)}
            {/* affiche la categorie */}
            <div style={{display : (showCategorie) ? "block" : "none"}}>
                <div className={`bg-${catColor} bg-opacity-40 flex justify-center `}>
                    <h1  className={` block text-xl font-bold mr-4`}>{cat.intitule}</h1>
                    <img className="w-8 block" src={`${process.env.PUBLIC_URL}/img/img-${cat.codeCategorie.toLowerCase()}.png`}/>
                </div>
                
            </div>
            {/* affiche les sous-categories */}
            <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3">
            {filteredList.map((scat,index)=> 
                <div className={`p-2 text-left shadow-lg rounded-md border-t-4 border-${catColor} m-2`}>{scat.intitule}</div>)
            }
            </div>
        </div>
    )
}

export default GroupeCompetence
