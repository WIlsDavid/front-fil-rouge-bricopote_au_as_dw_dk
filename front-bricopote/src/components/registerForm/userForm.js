import React from 'react'
import {Field, ErrorMessage} from 'formik'
import * as Yup from 'yup'
import TextErreur from '../Errors/TextErreur'

const inputStyle = 'py-2 px-3 rounded-lg border-2 border-yellow-600 focus:outline-none focus:ring-2 focus:ring-offset-yellow-600 focus:border-transparent';
const labelStyle = "uppercase md:text-sm text-xs text-gray-500 text-light font-semibold";


export const UserForm = () => {
    return (

        <div className="grid grid-cols-1 md:grid-cols-2 gap-5 md:gap-8 mt-5 mx-7">
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Nom</label>
                <Field className={inputStyle} type="text" placeholder="John" id='nom' name='user.nom' />
                <ErrorMessage component={TextErreur} name="user.nom" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Prenom</label>
                <Field className={inputStyle} type="text" placeholder="wick" id='prenom' name='user.prenom' />
                <ErrorMessage component={TextErreur} name="user.prenom" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Date de naissance</label>
                <Field className={inputStyle} type="date" placeholder="Selectionez une date" id='dateNaissance' name='user.dateNaissance' />
                <ErrorMessage component={TextErreur} name="user.dateNaissance" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Email</label>
                <Field className={inputStyle} type="Email" placeholder="JohnWick@exemple.fr" id='email' name='email' />
                <ErrorMessage component={TextErreur} name="email" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Tel</label>
                <Field className={inputStyle} type="text" placeholder="0678945120" id='tel' name='user.tel' />
                <ErrorMessage component={TextErreur} name="user.tel" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Mot de passe</label>
                <Field className={inputStyle} type="password" id='pwd' name='pwd' />
                <ErrorMessage component={TextErreur} name="pwd" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Num rue</label>
                <Field className={inputStyle} type="text" placeholder="6" id='numero' name='user.adresse.numero' />
                <ErrorMessage component={TextErreur} name="user.adresse.numero" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Libelle Rue</label>
                <Field className={inputStyle} type="text" placeholder="Rue du bateau" id='libelle' name='user.adresse.libelle' />
                <ErrorMessage component={TextErreur} name="user.adresse.libelle" />
            </div>
            <div className="grid grid-cols-1">
                <label className={labelStyle}>Code postal</label>
                <Field className={inputStyle} type="text" placeholder="75000" id='cp' name='user.adresse.cp' />
                <ErrorMessage component={TextErreur} name="user.adresse.cp" />
            </div>
            <div className="grid grid-cols-1">
                <label className="uppercase md:text-sm text-xs text-gray-500 text-light font-semibold">Ville</label>
                <Field className={inputStyle} type="text" placeholder="Paris" id='ville' name='user.adresse.ville' />
                <ErrorMessage component={TextErreur} name="user.adresse.ville" />
            </div>
        </div>
    )
}

export default UserForm
