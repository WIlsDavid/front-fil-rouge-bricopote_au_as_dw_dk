import React, { useState,useEffect ,Fragment} from 'react';
import UserSearchBar from './UserSearchBar';
import ProfileCard from './ProfileCard';
import Pagination from './Pagination'
import Axios from "axios";

const SearchUser = () => {
    
    const [bricoleurList,setBricoleurList] = useState([])
    const [page,setPage] = useState(0)
    const [nbResults,setNbResults] = useState(10)
    const [nbPages,setNbPages] = useState(0)
    const [params,setParams] = useState('cat=&ville=&key=')

    useEffect(() => {
        reload()
    },[page])

    useEffect(() => {
        console.log(params);
        reload_nbPages()
      },[params]);


    const reload_nbPages = () => {
        Axios
          .get(`http://localhost:8080/api/bricoleur/nb?${params}`)
          .then((res) => {
            setNbPages(Math.ceil(res.data / nbResults));
            setPage(0);
            reload();
            console.log('page ',page);
          })
          .catch((err) => console.log("error nbPage"));
    }

    const reload = () => {
        Axios.get(`http://localhost:8080/api/bricoleur/search?page=${page}&nbResults=${nbResults}&${params}`).then(res => setBricoleurList(res.data));
    }


    const display_results = () => {
        if(bricoleurList.length > 0){
            return (
               <>
                   <div className=" grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
                       {bricoleurList.map((brico,index) => (<ProfileCard key={index} bricopot={brico}/>))}
                   </div>
                   <Pagination page={page} setPage={setPage} nbPages={nbPages}/>
               </>
           )
       } else {
           return <p className="text-center text-2xl text-gray-700 my-52 text-" style={{textShadow:'2px 3px 10px black'}}>Désolé, Il n'y a pas de resultats.</p>

       }
    }

    return(
            <div className="mx-auto my-10 w-9/12">
                <UserSearchBar setParams={setParams} />
                {
                    display_results()
                }    
            </div>
            )
}

export default SearchUser;
