import React, { useState } from 'react';
import ServiceAuth from '../../services/ServiceAuth';
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import SelectionCategorie from '../composantsChoisirCompetences/SelectionCategorie';
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import modelValueRegisterForm from './modelValueRegisterForm';
import UserForm from './userForm';

const RegisterForm = (props) => {

    const errosCompetence = false;
    const step = ['Inscription', 'Sélectionnez vos compétences !!']
    const [steps, setSteps] = useState(1);
    const [listCompetence, setlistCompetence] = useState([]);
    const id = props.match.params.id;
    let history = useHistory();

    const backbtnStyle = 'w-auto bg-gray-500 hover:bg-gray-700 rounded-lg shadow-xl font-medium text-white px-4 py-2';

    function nextStep() {
        setSteps(steps => steps + 1)
    }
    function prevStep() {
        
        setSteps(steps => steps - 1)
        
    }

    const toShort = ' doit faire plus de 2 caractères';
    const toLong = 'doit faire moins de 25 caractères';
    const obligatoire = 'Le champs est obligatoire !! ';
    const validationSchema = Yup.object().shape({

        email: Yup.string()
            .required(obligatoire)
            .email('Email invalide'),

        pwd: Yup.string()
            .required(obligatoire)
        ,
        user: Yup.object().shape({

            nom: Yup.string()
                .min(2, toShort)
                .max(25, toLong)
                .required(obligatoire),

            prenom: Yup.string()
                .min(2, toShort)
                .max(25, toLong)
                .required(obligatoire),

            tel: Yup.string()
                .matches("0[1-9][0-9]{8}", 'Le numéro de télèphone doit être correct')
                .required(obligatoire),

            dateNaissance: Yup.date().required(obligatoire),

            adresse: Yup.object().shape({

                numero: Yup.string().required(obligatoire),
                libelle: Yup.string().required(obligatoire),
                cp: Yup.string().required(obligatoire).max(6, 'doit faire moins de 6 chiffre'),
                ville: Yup.string().required(obligatoire)

            })

        })
    })

    const onSubmit = values => {

        values.user.competences = listCompetence;
        values.user.email = values.email;
        console.log(values)

        if (id === '1') {

            ServiceAuth.createUser(values).then(response => {
                console.log(response);
            })
                .catch(function (erreur) {
                    console.log(erreur);
                });
            history.push('/')

        } else if (values.user.competences.length < 1) {

            errosCompetence = true;
            console.log(errosCompetence)
        } else {

            ServiceAuth.createBricopote(values).then(response => {
                console.log(response);
            })
                .catch(function (erreur) {
                    console.log(erreur);
                });
            history.push('/')
        }

    }

    return (
        <>
            <Formik
                initialValues={modelValueRegisterForm}
                onSubmit={steps < step.length && id === '0' ? nextStep : onSubmit}
                validationSchema={validationSchema}
            >
                <div className=" flex items-center justify-center  mt-32 mb-32">
                    <div className="grid bg-white  rounded-lg shadow-xl w-11/12 md:w-9/12 lg:w-1/2">
                        <Form >

                            <div className="flex justify-center">
                                <div className="flex">
                                    <h1 className="text-gray-600 font-bold md:text-2xl text-xl">{step[steps - 1]}</h1>
                                </div>
                            </div>
                            
                            {steps > step.length - 1 && id === '0' ? <SelectionCategorie listCompetence={listCompetence} setlistCompetence={setlistCompetence} /> : <UserForm />}

                            {listCompetence.length < 1 && steps == step.length ? <div className="text-red-600 text-center font-semibold">
                                Veuillez selectionez au moins une compétence
                            </div> : null}

                            <div className='flex items-center justify-center  md:gap-8 gap-4 pt-5 pb-5'>
                                {steps > 1 ?
                                    <input type="button" value={"Précendent"} onClick={prevStep} className={backbtnStyle} /> : <Link to='/'><button className={backbtnStyle}>Annuler</button></Link>
                                }
                                <input type="submit" value={steps === step.length || id === "1" ? "Valider" : "Suivant"} className='w-auto bg-green-600 hover:bg-green-800 rounded-lg shadow-xl font-medium text-white px-4 py-2' />
                            </div>
                        </Form>
                    </div>
                </div>

            </Formik >

        </>
    )


}

export default RegisterForm;