import React, { useEffect, useState, Fragment } from "react";
import Axios from "axios";

const CatDropDowunList = ({style,setCat}) => {
  const [categories, setCategories] = useState([]);

  const gopt_style =
    "cursor-pointer  border-gray-100 border-b hover:bg-teal-100 flex w-full items-center p-2 pl-2 border-transparent bg-white border-l-2 relative hover:bg-teal-600 hover:text-teal-100 border-teal-600 font-bold italic border-t-4";

    const opt_style =
    "cursor-pointer  border-gray-100 border-b hover:bg-teal-100 flex w-full items-center p-2 pl-2 border-transparent bg-white border-l-2 relative hover:bg-teal-600 hover:text-teal-100 border-teal-600 pl-10";

  useEffect(() => {
    Axios.get("http://127.0.0.1:8080/api/categories")
      .then((res) => setCategories(res.data))
      .catch((err) =>
        console.log("ERROR :", err, " Chargement de la liste FAILD")
      );
  }, []);

  const handle = ({target}) => {
    console.log(target);
      setCat(target.value)
  }

  return (
    <div className={`${style} shadow-inner`}>
      <select  name="cat" onChange={handle}>
          <option className={opt_style} value="">--Catégorie--</option>
        {categories.map((cat, index) => {
          return (
            <Fragment key={index}>
              <option className={gopt_style} value={cat.codeCategorie}>
                {cat.intitule}
              </option>
              {cat.listSousCategories.map((scat, index) => (
                <option className={opt_style} key={index} value={`${scat.codeSousCategorie}`}>
                  {`${scat.intitule}`}
                </option>
              ))}
            </Fragment>
          );
        })}
      </select>
    </div>
  );
};

export default CatDropDowunList;
