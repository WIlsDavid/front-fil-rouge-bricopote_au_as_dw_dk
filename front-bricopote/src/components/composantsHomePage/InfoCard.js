import React from 'react'
import SelectCatImage from '../../Logo/logoCards1/selectCat.svg'
import RecevezReponse from '../../Logo/logoCards1/recevezReponse.svg'
import PosterAnnonce from '../../Logo/logoCards1/posterAnnonce.svg'

const InfoCard = ({image ,texte}) => {

    const afficheImage = (image) => {

        switch (image) {
            case "selectCat":
                return <img className="w-32 m-auto" src={SelectCatImage}/>
                break;
            case "recevezReponse":
                return <img className="w-32 m-auto" src={RecevezReponse}/>
                break;
            case "posterAnnonce":
                return <img className="w-36 m-auto" src={PosterAnnonce}/>
                break;
        
            default:
                break;
        }
    }


    return (
        <div className="w-full grid grid-cols-1 ">
            {afficheImage(image)}
            <div className="m-auto text-center text-xl font-bold mb-6">{texte}</div>
        </div>
    )
}

export default InfoCard
