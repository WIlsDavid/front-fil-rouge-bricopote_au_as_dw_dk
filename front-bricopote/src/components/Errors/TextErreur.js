import React from 'react'

export const TextErreur = (props) => {
    return (
        <div className="text-red-600  font-semibold">
            {props.children}
        </div>
    )
}

export default TextErreur
