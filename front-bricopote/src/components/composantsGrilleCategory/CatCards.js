import React from 'react'
import { Link } from 'react-router-dom'

const CatCards = props => {




    return (
        <>
            <Link to={{ pathname: '/search', infoCard: props.cat }}>
                <div className={`text-center rounded-lg shadow-lg w-full md:w-48 h-40 bg-white hover:bg-${props.cat.codeCategorie.toLowerCase()} hover:text-gray-100 grid grid-cols-1`}>
                    <div className="m-auto">
                        <div className={`rounded-full  bg-${props.cat.codeCategorie.toLowerCase()} w-16 h-16 flex justify-center items-center`}>
                            <img className="w-10" src={`${process.env.PUBLIC_URL}/img/img-${props.cat.codeCategorie.toLowerCase()}.png`} />
                        </div>
                    </div>

                    <p className="m-auto text-center text-xl font-bold">{props.cat.intitule}</p>
                </div>
            </Link>
        </>
    )
}

export default CatCards

